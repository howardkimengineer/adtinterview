package com.example.intdemo.repository

import androidx.test.platform.app.InstrumentationRegistry
import com.example.intdemo.data.NetworkResponse
import com.example.intdemo.di.AppModule
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class RickAndMortyRepositoryInstrumentationTest {

    lateinit var rickRepo: RickAndMortyRepository

    @Before
    fun setup() {
        AppModule.initialize(InstrumentationRegistry.getInstrumentation().targetContext)
        rickRepo = RickAndMortyRepository()
    }

    @Test
    fun fetchEpisode_testPage1IsReturned() = runBlocking {
        // When
        val response = rickRepo.fetchEpisodes(1)

        // Then
        assertTrue(response is NetworkResponse.Success)
    }
}