package com.example.intdemo.di

import com.example.intdemo.network.RickAndMortyApi
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


// TODO Sept 11, 2020 Replace with actual DI framework later
object NetworkModule {

    val gson: Gson by lazy { Gson() }

    private val okHttpClient by lazy {
        OkHttpClient.Builder().build()
    }

    // TODO Sept 11, 2020 Inject url through environment variable when you have more time
    private val retrofit by lazy {
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .baseUrl("https://rickandmortyapi.com/api/")
            .build()
    }

    val rickAndMortyApi by lazy {
        retrofit.create(RickAndMortyApi::class.java)
    }
}
