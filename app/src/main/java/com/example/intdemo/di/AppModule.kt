package com.example.intdemo.di

import android.content.Context
import android.content.SharedPreferences
import com.example.intdemo.persistence.RickAndMortyLocalSource
import com.example.intdemo.persistence.RickAndMortyLocalSourceContract
import com.example.intdemo.repository.RickAndMortyRepository
import com.example.intdemo.repository.RickAndMortyRepositoryContract

const val RICK_AND_MORTY_SHARED = "key:rickAndMorty"

// TODO September 11, 2020 Use DI to bind context to app module in the future
object AppModule {

    @Volatile
    var context: Context? = null
    val rickAndMortyRepository: RickAndMortyRepositoryContract by lazy {
        RickAndMortyRepository()
    }
    val rickAndMortyLocalSource: RickAndMortyLocalSourceContract by lazy {
        RickAndMortyLocalSource()
    }
    val sharedPreferences: SharedPreferences by lazy {
        context!!.getSharedPreferences(RICK_AND_MORTY_SHARED, Context.MODE_PRIVATE)
    }

    fun initialize(context: Context) {
        if (this.context == null) {
            synchronized(this) {
                if (this.context == null) {
                    this.context = context
                }
            }
        }
    }
}