package com.example.intdemo

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.intdemo.data.EpisodesResponse
import com.example.intdemo.data.NetworkResponse
import com.example.intdemo.data.Result
import com.example.intdemo.di.AppModule
import com.example.intdemo.repository.RickAndMortyRepositoryContract
import kotlinx.coroutines.launch

class EpisodesViewModel(
    @VisibleForTesting val rickRepo: RickAndMortyRepositoryContract = AppModule.rickAndMortyRepository
): ViewModel() {

    @VisibleForTesting var page: Int = 1
    @VisibleForTesting val cache: LinkedHashSet<Result> = linkedSetOf()
    @VisibleForTesting val episodes: MutableLiveData<NetworkResponse<List<Result>>> = MutableLiveData()
    @VisibleForTesting var isLoading: Boolean = false

    fun fetchEpisodes() {

        viewModelScope.launch {
            if (!isLoading) {
                isLoading = true
                episodes.postValue(NetworkResponse.Loading())
                when (val response = rickRepo.fetchEpisodes(page)) {
                    is NetworkResponse.Success -> {
                        cache.addAll(response.data.results)
                        episodes.postValue(NetworkResponse.Success(cache.toList()))
                    }
                    is NetworkResponse.Error -> episodes.postValue(NetworkResponse.Error(response.error))
                }
                page += 1
                isLoading = false
            }
        }
    }
}