package com.example.intdemo

import android.app.Application
import com.example.intdemo.di.AppModule

class RickAndMortyApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        AppModule.initialize(this)
    }
}