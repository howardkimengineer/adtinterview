package com.example.intdemo.network

import com.example.intdemo.data.EpisodesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RickAndMortyApi {

    @GET("episode")
    suspend fun fetchEpisodes(@Query("page") page: Int): EpisodesResponse
}