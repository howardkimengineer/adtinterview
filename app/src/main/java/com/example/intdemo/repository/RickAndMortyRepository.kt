package com.example.intdemo.repository

import androidx.annotation.VisibleForTesting
import com.example.intdemo.data.EpisodesResponse
import com.example.intdemo.data.NetworkResponse
import com.example.intdemo.di.AppModule
import com.example.intdemo.di.NetworkModule
import com.example.intdemo.network.RickAndMortyApi
import com.example.intdemo.persistence.RickAndMortyLocalSource
import com.example.intdemo.persistence.RickAndMortyLocalSourceContract
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface RickAndMortyRepositoryContract {

    suspend fun fetchEpisodes(page: Int): NetworkResponse<EpisodesResponse>
}

class RickAndMortyRepository(
    @VisibleForTesting val rickAndMortyApi: RickAndMortyApi = NetworkModule.rickAndMortyApi,
    @VisibleForTesting val rickAndMortyLocal: RickAndMortyLocalSourceContract = AppModule.rickAndMortyLocalSource
) : RickAndMortyRepositoryContract {


    override suspend fun fetchEpisodes(page: Int): NetworkResponse<EpisodesResponse> {
        val localResponse = rickAndMortyLocal.getEpisodeResponse(page)
        if (localResponse != null) {
            return NetworkResponse.Success(localResponse)
        }
        return withContext(Dispatchers.IO) {
            try {
                rickAndMortyApi.fetchEpisodes(page).run {
                    rickAndMortyLocal.saveEpisodeResponse(page, this)
                    return@withContext NetworkResponse.Success(this)
                }
            } catch (e: Throwable) {
                e.printStackTrace()
                NetworkResponse.Error<EpisodesResponse>(e)
            }
        }
    }


}