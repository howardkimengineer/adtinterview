package com.example.intdemo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.intdemo.data.Result

class EpisodesAdapter : RecyclerView.Adapter<EpisodesViewHolder>() {

    private val episodes: MutableList<Result> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LayoutInflater.from(parent.context).inflate(R.layout.item_episode, parent, false).run {
            EpisodesViewHolder(this)
        }

    override fun getItemCount() = episodes.size

    override fun onBindViewHolder(holder: EpisodesViewHolder, position: Int) {
        holder.bindEpisodeToView(episodes[position])
    }

    fun setEpisodes(episodes: List<Result>) {
        this.episodes.clear()
        this.episodes.addAll(episodes)
        notifyDataSetChanged()
    }

}