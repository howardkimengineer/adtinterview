package com.example.intdemo.views

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View
import android.widget.Toast
import com.example.intdemo.BuildConfig
import com.example.intdemo.R.string

class LinkSpan(val url: String) : ClickableSpan() {

    override fun updateDrawState(ds: TextPaint) {
        ds.color = Color.BLUE
        ds.isUnderlineText = true
    }

    override fun onClick(view: View) {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            view.context.startActivity(intent)
        } catch (e: Throwable) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
            Toast.makeText(view.context, string.character_link_error, Toast.LENGTH_LONG).show()
        }
    }

}