package com.example.intdemo.persistence

import android.content.SharedPreferences
import androidx.annotation.VisibleForTesting
import com.example.intdemo.data.EpisodesResponse
import com.example.intdemo.di.AppModule
import com.example.intdemo.di.NetworkModule
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface RickAndMortyLocalSourceContract {

    suspend fun getEpisodeResponse(page: Int): EpisodesResponse?

    suspend fun saveEpisodeResponse(page: Int, episodesResponse: EpisodesResponse)
}

class RickAndMortyLocalSource(
    @VisibleForTesting val gson: Gson = NetworkModule.gson,
    @VisibleForTesting val sharedPreferences: SharedPreferences = AppModule.sharedPreferences
) : RickAndMortyLocalSourceContract {

    override suspend fun getEpisodeResponse(page: Int): EpisodesResponse? {
        return withContext(Dispatchers.IO) {
            val storedResponse = sharedPreferences.getString(page.getPageKey(), "")
            if (storedResponse.isNullOrEmpty()) {
                return@withContext null
            } else {
                return@withContext gson.fromJson(storedResponse, EpisodesResponse::class.java)
            }
        }
    }

    override suspend fun saveEpisodeResponse(page: Int, episodesResponse: EpisodesResponse) {
        sharedPreferences.edit().putString(page.getPageKey(), gson.toJson(episodesResponse)).apply()
    }
}

fun Int.getPageKey() = "key:page${this}"