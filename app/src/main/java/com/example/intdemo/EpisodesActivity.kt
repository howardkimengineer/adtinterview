package com.example.intdemo

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.example.intdemo.data.NetworkResponse
import com.example.intdemo.data.Result
import kotlinx.android.synthetic.main.activity_main.*

// TODO September 11, 2020 replace with Timber
const val TAG = "EpisodesActLog"

class EpisodesActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProvider(this).get(EpisodesViewModel::class.java)
    }
    private val episodesAdapter by lazy {
        EpisodesAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setRecyclerView()
        observeViewModel()
        viewModel.fetchEpisodes()
    }

    private fun setRecyclerView() {
        episodesRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@EpisodesActivity)
            adapter = episodesAdapter
            addOnScrollListener(object : OnScrollListener() {

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (!recyclerView.canScrollVertically(1)) {
                        // We've reached the bottom
                        viewModel.fetchEpisodes()
                    }
                }
            })

        }
    }

    private fun observeViewModel() {
        viewModel.episodes.observe(this, Observer {
            when (it) {
                is NetworkResponse.Success -> onSuccess(it)
                is NetworkResponse.Error -> onError(it)
                is NetworkResponse.Loading -> episodesProgressBar.visibility = View.VISIBLE
            }
        })
    }

    private fun onSuccess(response: NetworkResponse.Success<List<Result>>) {
        episodesProgressBar.visibility = View.GONE
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Success: " + response.data.size)
        }
        episodesAdapter.setEpisodes(response.data)
    }

    private fun onError(response: NetworkResponse.Error<List<Result>>) {
        episodesProgressBar.visibility = View.GONE
        Toast.makeText(this, R.string.episodes_error, Toast.LENGTH_LONG).show()
        if (BuildConfig.DEBUG) {
            Log.e(TAG, response.error.localizedMessage ?: "")
        }
    }
}