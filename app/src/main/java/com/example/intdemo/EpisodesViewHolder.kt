package com.example.intdemo

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.intdemo.data.Result
import com.example.intdemo.views.LinkSpan
import kotlinx.android.synthetic.main.item_episode.view.*

class EpisodesViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    fun bindEpisodeToView(episode: Result) {
        view.cardTitle.text = episode.getTitle()
        setCharacters(episode)
    }

    private fun setCharacters(episode: Result) {
        val characterText = SpannableStringBuilder()
        var start = 0
        val breakCharacter = "\n"
        episode.characters.forEach { character ->
            characterText.append(character)
            characterText.setSpan(LinkSpan(character), start, start + character.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            characterText.append(breakCharacter)
            start += character.length + breakCharacter.length
        }
        view.cardCharacters.apply {
            text = characterText
            movementMethod = LinkMovementMethod.getInstance()
        }
    }

    private fun Result.getTitle() = "${this.episode}: ${this.name}"
}